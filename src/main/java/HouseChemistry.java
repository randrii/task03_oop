abstract class HouseChemistry {
    private int number;
    private String title;
    private double price;

    public HouseChemistry(int number, String title, double price) {
        this.number = number;
        this.title = title;
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }
}
