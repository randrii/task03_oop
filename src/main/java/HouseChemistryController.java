import java.util.*;

public class HouseChemistryController {

    private List<HouseChemistry> chemistries;
    private byte choise;
    private Scanner scanner;
    private List<HouseChemistry> tempChoise;

    public HouseChemistryController() {
        this.chemistries = new ArrayList<HouseChemistry>();
        this.scanner = new Scanner(System.in);

        HouseChemistry cleaningChemistry1 = new CleaningProduct(1, "Mr.Muscle", 23.10);
        HouseChemistry cleaningChemistry2 = new CleaningProduct(2, "Gala", 15.50);
        HouseChemistry cleaningChemistry3 = new CleaningProduct(3, "Mr.Proper", 25.20);
        HouseChemistry dishwasherChemistry1 = new Dishwasher(4, "Fairy", 19.90);
        HouseChemistry dishwasherChemistry2 = new Dishwasher(5, "Somat", 54.90);
        HouseChemistry dishwasherChemistry3 = new Dishwasher(6, "Gala", 32.90);
        HouseChemistry powderChemistry1 = new WashingPowder(7, "Perwoll", 269.90);
        HouseChemistry powderChemistry2 = new WashingPowder(8, "Persil", 109);
        HouseChemistry powderChemistry3 = new WashingPowder(9, "Ariel", 99.60);

        chemistries.add(cleaningChemistry1);
        chemistries.add(cleaningChemistry2);
        chemistries.add(cleaningChemistry3);
        chemistries.add(dishwasherChemistry1);
        chemistries.add(dishwasherChemistry2);
        chemistries.add(dishwasherChemistry3);
        chemistries.add(powderChemistry1);
        chemistries.add(powderChemistry2);
        chemistries.add(powderChemistry3);
    }

    public void showItems() {
        HouseChemistryView.printItems(chemistries);
    }

    public void showMenu() {
        HouseChemistryView.printMenu();

        this.choise = scanner.nextByte();
        switch (choise) {
            case 1: {
                showItems();
                break;
            }
            case 2: {
                showItems();
                System.out.print("Input No. of items, indenting by comma: ");
                String strChoise = scanner.next();
                setChoise(strChoise);
                printChosenItems();
            }
            default: {
                System.out.println("Wrong operation number!");
            }
        }
    }

    public void setChoise(String choise) {
        String[] stringItems = choise.split(",");
        int[] items = new int[stringItems.length];

        for (int i = 0; i < stringItems.length; i++) {
            items[i] = Integer.parseInt(stringItems[i]);
        }

        tempChoise = new ArrayList<HouseChemistry>();

        for (int i = 0; i < items.length; i++) {
            for (HouseChemistry chemistry : chemistries) {
                if (chemistry.getNumber() == items[i]) {
                    tempChoise.add(chemistry);
                }
            }
        }

    }

    private void printChosenItems() {
        Collections.sort(tempChoise, new Comparator<HouseChemistry>() {
            public int compare(HouseChemistry chemistry1, HouseChemistry chemistry2) {
                return Double.compare(chemistry1.getPrice(), chemistry2.getPrice());
            }
        });

        for (HouseChemistry chemistry : tempChoise) {
            System.out.println("No. " + chemistry.getNumber()
                    + "\t Title:" + chemistry.getTitle()
                    + "\t Type: " + chemistry.getClass().getSimpleName()
                    + "\t Price: " + chemistry.getPrice());
        }

    }
}
