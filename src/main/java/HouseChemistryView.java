import java.util.List;

public class HouseChemistryView {
    public static void printItems(List<HouseChemistry> chemistries) {
        for (HouseChemistry houseChemistry : chemistries) {
            System.out.println("No. " + houseChemistry.getNumber() + "\tTitle : "
                    + houseChemistry.getTitle() + "\tPrice : "
                    + houseChemistry.getPrice());
        }
    }

    public static void printMenu() {
        System.out.print("1.Print all items.\n2.Choose items.\n\nPlease input choise (1-3): ");
    }
}
